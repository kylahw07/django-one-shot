from django.urls import path
from todos.views import (
    todolist_view,
    todolist_detail,
    create_todolist,
    todo_list_update,
)

urlpatterns = [
    path("create/", create_todolist, name="todo_list_create"),
    path("todos/detail/<int:id>/", todolist_detail, name="todo_list_detail"),
    path("", todolist_view, name="todo_list_list"),
    path("todos/<int:id>/edit/", todo_list_update, name="todo_list_update"),
]
