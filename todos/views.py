from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm


# Create your views here.
def todolist_view(request):
    todolist = TodoList.objects.all()
    context = {
        "todolist": todolist,
    }
    return render(request, "todos/todolist.html", context)


def todolist_detail(request, id):
    todolist = TodoList.objects.get(id=id)
    context = {
        "todolist": todolist,
    }
    return render(request, "todos/detail.html", context)


def create_todolist(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail")
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    lists = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=lists)
        if form.is_valid():
            lists = form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=lists)

    context = {
        "form": form,
        "lists": lists,
    }
    return render(request, "todos/update.html", context)
